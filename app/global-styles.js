import { injectGlobal } from 'styled-components'
/* eslint no-unused-expressions: 0 */

injectGlobal` html,
body {
  height: 100%;
  width: 100%;
}

h2 {
  font-size: 16px !important;
}

body {
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
}

body.fontLoaded {
  font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
}

.margin-side {
  margin: 0px 20px;
}

.pull-left {
  text-align: left;
}

.pull-right {
  text-align: right;
}

#app {
  background-color: #fafafa;
  min-height: 100%;
  min-width: 100%;
}

a {
  color: #448AFF;
  text-decoration: none !important;
}

p,
label {
  font-family: Georgia, Times, 'Times New Roman', serif;
  line-height: 1.5em;
}
`
