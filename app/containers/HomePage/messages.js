/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage component.
 */
import { defineMessages } from 'react-intl'
import constants from './constants'

export default defineMessages({
  history: {
    id: constants.historyButton,
    defaultMessage: constants.historyButtonDefault
  },
  latest: {
    id: constants.latest,
    defaultMessage: constants.latest
  }
})
