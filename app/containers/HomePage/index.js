/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react'
import { Typography } from '@material-ui/core'
import { FormattedMessage } from 'react-intl'

import Latest from '../../components/Latest'
import History from '../../components/History'
import messages from './messages'
import './style.less'

/* eslint-disable react/prefer-stateless-function */
export default class HomePage extends React.PureComponent {
  render () {
    return (
      <div>
        <Typography variant="title">
          <FormattedMessage {...messages.history} />
        </Typography>
        <History />

        <Typography variant="title">
          <FormattedMessage {...messages.latest} />
        </Typography>
        <Latest />
      </div>
    )
  }
}
