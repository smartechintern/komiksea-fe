module.exports = {
  indexDescribe1: '<HomePage />',
  indexMessage1: 'should render Home',
  historyButton: 'app.components.button.history',
  historyButtonDefault: 'History',
  latest: 'app.components.latest',
  latestDefault: 'Latest'
}
