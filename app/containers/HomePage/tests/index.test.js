import React from 'react'
// import { FormattedMessage } from 'react-intl'
// import messages from '../messages'
import { shallow } from 'enzyme'
import { Typography } from '@material-ui/core'
import { FormattedMessage } from 'react-intl'

import messages from '../messages'
import HomePage from '../index'
import constants from '../constants'
import Latest from '../../../components/Latest'
import History from '../../../components/History'

const renderedComponent = shallow(<HomePage />)
describe(constants.indexDescribe1, () =>
  it(constants.indexMessage1, () =>
    expect(renderedComponent.contains(<div>
      <Typography variant="title">
        <FormattedMessage {...messages.history} />
      </Typography>
      <History />

      <Typography variant="title">
        <FormattedMessage {...messages.latest} />
      </Typography>
      <Latest />
    </div>)).toEqual(true)))

describe(constants.indexDescribe1, () =>
  it(constants.indexDescribe1, () =>
    expect(constants).toEqual({
      indexDescribe1: '<HomePage />',
      indexMessage1: 'should render Home',
      historyButton: 'app.components.button.history',
      historyButtonDefault: 'History',
      latest: 'app.components.latest',
      latestDefault: 'Latest'
    })))
