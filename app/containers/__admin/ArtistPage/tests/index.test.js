// import { FormattedMessage } from 'react-intl'
// import messages from '../messages'

import constants from '../constants'

describe(constants.indexDescribe1, () =>
  it(constants.indexDescribe1, () =>
    expect(constants).toEqual({
      indexDescribe1: '<ListPage />',
      indexMessage1: 'should render List',
      comicsList: 'app.components.list.comics',
      comicsListDefault: 'History',
      search: 'app.components.text.searchBy',
      searchDefault: 'Search By',
      sort: 'app.components.text.sortBy',
      sortDefault: 'Sort By'
    })))
