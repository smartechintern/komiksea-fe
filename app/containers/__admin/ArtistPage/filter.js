import React from 'react'
import PropTypes from 'prop-types'
import { Grid, TextField, InputAdornment, Button } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import { withStyles } from '@material-ui/core/styles'
import './style.less'


/* eslint-disable react/prefer-stateless-function */
class Filter extends React.PureComponent {
  render () {
    const { classes, width } = this.props

    return (
      <Grid container>
        <Grid item xs={12} sm={6}>
          <Button key="1" variant="contained" color="primary" className={classes.margin} style={{ width }}>Add</Button>
        </Grid>
        <Grid style={{ float: 'right' }} item xs={12} sm={6}>
          <TextField
            style={{ float: 'right' }}
            className={classes.margin}
            InputLabelProps={{
              classes: {
                root: classes.cssLabel,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              )
            }}
            label="Search"
            variant="outlined"
            id="custom-css-outlined-input"
          />
        </Grid>
      </Grid>
    )
  }
}

Filter.propTypes = {
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired
}

export default withStyles()(Filter)
