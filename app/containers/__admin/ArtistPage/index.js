/*
 * Artist Page
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react'
import Filter from './filter'
// import { FormattedMessage } from 'react-intl'

import { ToggleButtonGroup, ToggleButton } from '@material-ui/lab'

import Table from '../../../components/Table'
import { formatDate } from '../../../utils/formatDate'
// import messages from './messages'
import './style.less'

/* eslint-disable react/prefer-stateless-function */
export default class ArtistPage extends React.PureComponent {
  render () {
    const tableProps = {
      dataSource: [
        {
          id: 11,
          image: '/images/users/default.png',
          name: 'Eiichiro Oda11',
          description: 'Artist and Author of One Piece',
          slug: 'eiichiro-oda11',
          birthDate: '1975-01-31',
          createdBy: '1',
          updatedBy: null,
          deletedBy: null,
          createdAt: '1542910651000'
        },
        {
          id: 10,
          image: '/images/users/default.png',
          name: 'Eiichiro Oda10',
          description: 'Artist and Author of One Piece',
          slug: 'eiichiro-oda10',
          birthDate: '1975-01-31',
          createdBy: '1',
          updatedBy: null,
          deletedBy: null,
          createdAt: '1542910651000'
        },
        {
          id: 9,
          image: '/images/users/default.png',
          name: 'Eiichiro Oda9',
          description: 'Artist and Author of One Piece',
          slug: 'eiichiro-oda9',
          birthDate: '1975-01-31',
          createdBy: '1',
          updatedBy: null,
          deletedBy: null,
          createdAt: '1542910651000'
        },
        {
          id: 8,
          image: '/images/users/default.png',
          name: 'Eiichiro Oda8',
          description: 'Artist and Author of One Piece',
          slug: 'eiichiro-oda8',
          birthDate: '1975-01-31',
          createdBy: '1',
          updatedBy: null,
          deletedBy: null,
          createdAt: '1542910651000'
        },
        {
          id: 7,
          image: '/images/users/default.png',
          name: 'Eiichiro Oda7',
          description: 'Artist and Author of One Piece',
          slug: 'eiichiro-oda7',
          birthDate: '1975-01-31',
          createdBy: '1',
          updatedBy: null,
          deletedBy: null,
          createdAt: '1542910651000'
        },
        {
          id: 6,
          image: '/images/users/default.png',
          name: 'Eiichiro Oda6',
          description: 'Artist and Author of One Piece',
          slug: 'eiichiro-oda6',
          birthDate: '1975-01-31',
          createdBy: '1',
          updatedBy: null,
          deletedBy: null,
          createdAt: '1542910651000'
        },
        {
          id: 5,
          image: '/images/users/default.png',
          name: 'Eiichiro Oda5',
          description: 'Artist and Author of One Piece',
          slug: 'eiichiro-oda5',
          birthDate: '1975-01-31',
          createdBy: '1',
          updatedBy: null,
          deletedBy: null,
          createdAt: '1542910651000'
        },
        {
          id: 4,
          image: '/images/users/default.png',
          name: 'Eiichiro Oda4',
          description: 'Artist and Author of One Piece',
          slug: 'eiichiro-oda4',
          birthDate: '1975-01-31',
          createdBy: '1',
          updatedBy: null,
          deletedBy: null,
          createdAt: '1542910651000'
        },
        {
          id: 3,
          image: '/images/users/default.png',
          name: 'Eiichiro Oda3',
          description: 'Artist and Author of One Piece',
          slug: 'eiichiro-oda3',
          birthDate: '1975-01-31',
          createdBy: '1',
          updatedBy: null,
          deletedBy: null,
          createdAt: '1542910651000'
        },
        {
          id: 2,
          image: '/images/users/default.png',
          name: 'Eiichiro Oda2',
          description: 'Artist and Author of One Piece',
          slug: 'eiichiro-oda2',
          birthDate: '1975-01-31',
          createdBy: '1',
          updatedBy: null,
          deletedBy: null,
          createdAt: '1542910651000'
        },
        {
          id: 1,
          image: '/images/users/default.png',
          name: 'Eiichiro Oda1',
          description: 'Artist and Author of One Piece',
          slug: 'eiichiro-oda1',
          birthDate: '1975-01-31',
          createdBy: '1',
          updatedBy: null,
          deletedBy: null,
          createdAt: '1542910651000'
        }
      ],
      columns: [
        {
          title: 'Action',
          key: 'action',
          dataIndex: 'action',
          render: () => (
            <div>
              <ToggleButtonGroup value="left" exclusive>
                <ToggleButton value="left">
                  Edit
                </ToggleButton>
                <ToggleButton value="center">
                  Del
                </ToggleButton>
              </ToggleButtonGroup>
            </div>
          )
        },
        {
          title: 'ID',
          dataIndex: 'id',
          key: 'id'
        },
        {
          title: 'Image',
          dataIndex: 'image',
          key: 'image'
        },
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name'
        },
        {
          title: 'Description',
          dataIndex: 'description',
          key: 'description'
        },
        {
          title: 'Slug',
          dataIndex: 'slug',
          key: 'slug'
        },
        {
          title: 'Birth Date',
          dataIndex: 'birthDate',
          key: 'birthDate',
          render: text => formatDate(text, 'YYYY-MM-DD')
        },
        {
          title: 'CreatedBy',
          dataIndex: 'createdBy',
          key: 'createdBy'
        },
        {
          title: 'UpdatedBy',
          dataIndex: 'updatedBy',
          key: 'updatedBy'
        }
      ]
    }
    return (
      <div>
        <Filter />
        <Table {...tableProps} />
      </div>
    )
  }
}
