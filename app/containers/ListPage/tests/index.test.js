import React from 'react'
// import { FormattedMessage } from 'react-intl'
// import messages from '../messages'
import { shallow } from 'enzyme'
import { Typography, Card, CardContent, Grid } from '@material-ui/core'
import { FormattedMessage } from 'react-intl'

import messages from '../messages'
import HomePage from '../index'
import constants from '../constants'
import ListItem from '../../../components/ListItem'

const renderedComponent = shallow(<HomePage />)
describe(constants.indexDescribe1, () =>
  it(constants.indexMessage1, () =>
    expect(renderedComponent.contains(<div>
      <Grid container spacing={24}>
        <Grid item xs={6} sm={3}>
          <Typography variant="title">
            <FormattedMessage {...messages.list} />
          </Typography>
        </Grid>
        <Grid item xs={6} sm={3} />
        <Grid item xs={6} sm={3} />
        <Grid item xs={6} sm={3}>
          <Typography variant="title">
            <FormattedMessage {...messages.search} />
          </Typography>
        </Grid>
      </Grid>

      <Card className="card">
        <CardContent className="container-card">
          <Typography gutterBottom>
            <FormattedMessage {...messages.sort} />
          </Typography>
        </CardContent>
      </Card>

      <ListItem />
    </div>)).toEqual(true)))

describe(constants.indexDescribe1, () =>
  it(constants.indexDescribe1, () =>
    expect(constants).toEqual({
      indexDescribe1: '<ListPage />',
      indexMessage1: 'should render List',
      comicsList: 'app.components.list.comics',
      comicsListDefault: 'History',
      search: 'app.components.text.searchBy',
      searchDefault: 'Search By',
      sort: 'app.components.text.sortBy',
      sortDefault: 'Sort By'
    })))
