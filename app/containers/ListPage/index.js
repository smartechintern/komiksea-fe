/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react'
import { Typography, Grid, Card, CardContent } from '@material-ui/core'
import { FormattedMessage } from 'react-intl'

import ListItem from '../../components/ListItem'
import messages from './messages'
import './style.less'

/* eslint-disable react/prefer-stateless-function */
export default class ListPage extends React.PureComponent {
  render () {
    return (
      <div>
        <Grid container spacing={24}>
          <Grid item xs={6} sm={3}>
            <Typography variant="title">
              <FormattedMessage {...messages.list} />
            </Typography>
          </Grid>
          <Grid item xs={6} sm={3} />
          <Grid item xs={6} sm={3} />
          <Grid item xs={6} sm={3}>
            <Typography variant="title">
              <FormattedMessage {...messages.search} />
            </Typography>
          </Grid>
        </Grid>

        <Card className="card">
          <CardContent className="container-card">
            <Typography gutterBottom>
              <FormattedMessage {...messages.sort} />
            </Typography>
          </CardContent>
        </Card>

        <ListItem />
      </div>
    )
  }
}
