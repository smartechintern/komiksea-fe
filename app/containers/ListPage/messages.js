/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage component.
 */
import { defineMessages } from 'react-intl'
import constants from './constants'

export default defineMessages({
  list: {
    id: constants.comicsList,
    defaultMessage: constants.comicsListDefault
  },
  search: {
    id: constants.search,
    defaultMessage: constants.searchDefault
  },
  sort: {
    id: constants.sort,
    defaultMessage: constants.sortDefault
  }
})
