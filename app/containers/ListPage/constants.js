module.exports = {
  indexDescribe1: '<ListPage />',
  indexMessage1: 'should render List',
  comicsList: 'app.components.list.comics',
  comicsListDefault: 'History',
  search: 'app.components.text.searchBy',
  searchDefault: 'Search By',
  sort: 'app.components.text.sortBy',
  sortDefault: 'Sort By'
}
