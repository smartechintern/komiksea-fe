/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react'
import { Switch, Route, withRouter } from 'react-router-dom'

import { withStyles } from '@material-ui/core/styles'
import { List } from '@material-ui/core'
import HomePage from 'containers/HomePage/Loadable'
import ListPage from 'containers/ListPage/Loadable'
import ArtistPage from 'containers/__admin/ArtistPage/Loadable'
import NotFoundPage from 'containers/NotFoundPage/Loadable'
import './style.less'

import Header from '../../components/Header'
import AdminDrawer from '../../components/AdminDrawer'

const SomeComponent = withRouter(props => <App {...props} />)
const styles = theme => ({
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('sm')]: {
      display: 'none'
    }
  }
})

class App extends React.PureComponent {
  state = {
    mobileOpen: true
  }

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }))
  }

  render () {
    const { mobileOpen } = this.state
    window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true
    const checkIfAdmin = () => this.props.location.pathname.substring(0, 6) === '/admin'

    const isAdmin = checkIfAdmin()

    return (
      <div className={isAdmin ? 'admin' : 'web'}>
        <Header
          leftLinks={
            <List className="list" />
          }
          handleDrawerToggle={this.handleDrawerToggle}
          isAdmin={isAdmin}
        />
        {mobileOpen && isAdmin && <AdminDrawer />}
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/home" component={HomePage} />
          <Route exact path="/list" component={ListPage} />

          <Route exact path="/admin" component={ArtistPage} />
          <Route exact path="/admin/artist" component={ArtistPage} />
          <Route exact path="/admin/author" component={ArtistPage} />
          <Route exact path="/admin/category" component={ArtistPage} />
          <Route exact path="/admin/comics" component={ArtistPage} />
          <Route exact path="/admin/comics/pages/:id" component={ArtistPage} />
          <Route exact path="/admin/users" component={ArtistPage} />

          <Route component={NotFoundPage} />
        </Switch>
      </div>
    )
  }
}

export default withStyles(styles)(SomeComponent)
