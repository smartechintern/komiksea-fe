import { fromJS } from 'immutable'

import { makeSelectLocation } from 'containers/App/selectors'

import constants from '../constants'

const route = fromJS({
  location: { pathname: constants.selectionIndex }
})
const mockedState = fromJS({
  route
})
describe(constants.selectionDescribe1, () =>
  it(constants.selectionMessage1, () =>
    expect(makeSelectLocation()(mockedState)).toEqual(route.get('location').toJS())))
