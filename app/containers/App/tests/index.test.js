// import React from 'react'
// import { shallow } from 'enzyme'
// import { Route } from 'react-router-dom'

import constants from '../constants'

// import App from '../index'

// const renderedComponent = shallow(<App />)

// describe(constants.indexDescribe1, () =>
//   it(constants.indexMessage1, () =>
//     expect(renderedComponent.find(Route).length).not.toBe(0)))

describe(constants.selectionDescribe1, () =>
  it(constants.selectionDescribe1, () =>
    expect(constants).toEqual({
      selectionDescribe1: 'makeSelectLocation',
      selectionMessage1: 'should select the location',
      selectionIndex: '/foo',
      indexDescribe1: '<App />',
      indexMessage1: 'should render some routes'
    })))
