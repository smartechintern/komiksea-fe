import moment from 'moment'

/**
 * @param {date} date
 * @returns {string}
 */
const formatDate = (date) => {
  return moment(date, 'YYYY-MMM-DD').format('DD-MMM-YYYY')
}

export {
  formatDate
}
