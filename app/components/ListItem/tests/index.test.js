import React from 'react'
import { shallow } from 'enzyme'
import { Grid, Card } from '@material-ui/core'
import { Link } from 'react-router-dom'

import History from '../index'
import history from '../mock/history'

const renderedComponent = shallow(<History />)

describe('<History />', () => {
  it('Should Render History', () => {
    expect(renderedComponent.contains(<div>
      <Grid className="paper" container spacing={24}>
        {history.map((data, index) => {
          return (
            <Grid key={index} className="margin-latest text-history" item xs={6}>
              <div className="margin-mini">
                <Link className="paper" to="/home" key={index}>
                  <Card className="card">
                    <img src={require(`!file-loader?name=[name].[ext]!../../mock/assets/history${index + 1}_250x350.jpg`)} style={{ width: '150px', height: '200px' }} alt="Images" />
                    {data.release_at}
                  </Card>
                </Link>
                <br />
                <br />
                {data.chapters.map((dataChapters, indexChapters) => {
                  if (indexChapters === 0) {
                    return (
                      <Link className="paper" to="/home" key={indexChapters}>
                        <div>
                          {`#${dataChapters.chapter}  ${dataChapters.title ? dataChapters.title : `chapter ${dataChapters.chapter}`}`}
                        </div>
                      </Link>
                    )
                  }
                  return ''
                })}
              </div>
            </Grid>
          )
        })}
      </Grid>
    </div>)).toEqual(true)
  })
})
