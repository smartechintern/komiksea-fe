import React from 'react'
import { shallow } from 'enzyme'
import { Grid } from '@material-ui/core'
import { Link } from 'react-router-dom'

import History from '../index'
import history from '../mock/history'

const renderedComponent = shallow(<History />)

describe('<History />', () => {
  it('Should Render History', () => {
    expect(renderedComponent.contains(
      <Grid className="paper" container spacing={16}>
        {history.map((data, index) => {
          return (
            <Grid key={index} className="margin-latest text-history" item xs={6} sm={4} md={3}>
              <div className="margin-mini">
                <Link className="paper" to="/home" key={index}>
                  <img src={require(`!file-loader?name=[name].[ext]!../../mock/assets/history${index + 1}_250x350.jpg`)} style={{ width: '150px', height: '200px' }} alt="Images" />
                </Link>
                <br />
                <br />
                {data.release_at}
                {data.chapters.map((dataChapters, indexChapters) => {
                  return (
                    <Link className="paper" to="/home" key={indexChapters}>
                      <div>
                        {`#${dataChapters.chapter}  ${dataChapters.title ? dataChapters.title : `chapter ${dataChapters.chapter}`}`}
                      </div>
                    </Link>
                  )
                })}
              </div>
            </Grid>
          )
        })}
      </Grid>
    )).toEqual(true)
  })
})
