const latest = [
  {
    title: 'Domestic na Kanojo',
    chapters: [
      {
        chapter: 145,
        title: 'Hubungan yang retak'
      }
    ],
    release_at: '2018-09-24'
  },
  {
    title: 'Koi to Uso',
    chapters: [
      {
        chapter: 165,
        title: null
      }
    ],
    release_at: '2018-09-23'
  },
  {
    title: 'One Piece',
    chapters: [
      {
        chapter: 167,
        title: null
      }
    ],
    release_at: '2018-09-23'
  },
  {
    title: 'Naruto',
    chapters: [
      {
        chapter: 700,
        title: 'Hokage ke-7'
      }
    ],
    release_at: '2018-09-23'
  }
]

export default latest
