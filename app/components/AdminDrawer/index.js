import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import { withStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import CssBaseline from '@material-ui/core/CssBaseline'
import List from '@material-ui/core/List'
import Divider from '@material-ui/core/Divider'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'

const drawerWidth = 240

const styles = theme => ({
  root: {
    display: 'flex'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3
  },
  toolbar: theme.mixins.toolbar
})

function ClippedDrawer (props) {
  const { classes } = props

  const listMenu = [
    {
      key: '2',
      name: 'Artist',
      route: '/admin/artist'
    },
    {
      key: '3',
      name: 'Author',
      route: '/admin/author'
    },
    {
      key: '4',
      name: 'Category',
      route: '/admin/category'
    },
    {
      key: '5',
      name: 'Comics',
      route: '/admin/comics'
    }
  ]

  const listCredential = [
    {
      key: '6',
      name: 'Users',
      route: '/admin/users'
    }
  ]

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <div className={classes.toolbar} />
        <List>
          {listMenu.map(data => (
            <Link to={data.route} key={data.key}>
              <ListItem button>
                <ListItemText primary={data.name} />
              </ListItem>
            </Link>
          ))}
        </List>
        <Divider />
        <List>
          {listCredential.map(data => (
            <Link to={data.route} key={data.key}>
              <ListItem button>
                <ListItemText primary={data.name} />
              </ListItem>
            </Link>
          ))}
        </List>
      </Drawer>
    </div>
  )
}

ClippedDrawer.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(ClippedDrawer)
