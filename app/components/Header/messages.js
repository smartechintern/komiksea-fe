/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage component.
 */
import { defineMessages } from 'react-intl'
import constants from './constants'

export default defineMessages({
  homeButton: {
    id: constants.homeButton,
    defaultMessage: constants.homeButtonDefault
  },
  listButton: {
    id: constants.listButton,
    defaultMessage: constants.loginButtonDefault
  },
  historyButton: {
    id: constants.historyButton,
    defaultMessage: constants.historyButtonDefault
  },
  loginButton: {
    id: constants.loginButton,
    defaultMessage: constants.loginButtonDefault
  }
})
