/**
 *
 * Header
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'
import { FormattedMessage } from 'react-intl'
import { AppBar, Button, Toolbar, Hidden, IconButton, Grid } from '@material-ui/core'
import { Link } from 'react-router-dom'
import Menu from '@material-ui/icons/Menu'

import './style.less'
import messages from './messages'

const styles = theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('sm')]: {
      display: 'none'
    }
  }
})

class Header extends React.PureComponent {
  render () {
    const {
      classes,
      leftLinks,
      isAdmin
    } = this.props
    const brandComponent = <Button className="title">Komiksea</Button>

    return (
      <div className="header-content">
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar className="header">
            <Hidden mdUp>
              <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={this.props.handleDrawerToggle}
                className={classes.menuButton}
              >
                <Menu />
              </IconButton>
            </Hidden>
            <Link to={isAdmin ? '/admin' : '/'}>
              {leftLinks ? brandComponent : null}
            </Link>
            <div className="flex">
              {leftLinks ? (
                <Hidden smDown implementation="css">
                  {leftLinks}
                </Hidden>
              ) : (brandComponent)}
            </div>
            {!isAdmin && (
              <Grid
                justify="space-between" // Add it here :)
                container
                spacing={24}
              >
                <Grid item />
                <Grid item>
                  <Link to="/home">
                    <Button color="inherit" className="button">
                      <FormattedMessage {...messages.homeButton} />
                    </Button>
                  </Link>
                  <Link to="/list">
                    <Button color="inherit" className="button">
                      <FormattedMessage {...messages.listButton} />
                    </Button>
                  </Link>
                  <Link to="/history">
                    <Button color="inherit" className="button">
                      <FormattedMessage {...messages.historyButton} />
                    </Button>
                    <Button color="inherit" variant="outlined" className="login button">
                      <FormattedMessage {...messages.loginButton} />
                    </Button>
                  </Link>
                </Grid>
              </Grid>)}
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

Header.propTypes = {
  leftLinks: PropTypes.node,
  classes: PropTypes.object.isRequired
}

Header.defaultProps = {
  leftLinks: null
}

export default withStyles(styles)(Header)
