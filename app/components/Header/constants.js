/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT'
 */
module.exports = {
  homeButton: 'app.components.button.home',
  homeButtonDefault: 'Home',
  listButton: 'app.components.button.list',
  listButtonDefault: 'List',
  historyButton: 'app.components.button.history',
  historyButtonDefault: 'History',
  loginButton: 'app.components.button.login',
  loginButtonDefault: 'Log in'
}
