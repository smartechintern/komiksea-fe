const latest = [
  {
    title: 'Domestic na Kanojo',
    chapters: [
      {
        chapter: 145,
        title: 'Hubungan yang retak'
      }
    ],
    release_at: '2018-09-24'
  },
  {
    title: 'Koi to Uso',
    chapters: [
      {
        chapter: 165,
        title: null
      },
      {
        chapter: 164,
        title: null
      },
      {
        chapter: 163,
        title: null
      }
    ],
    release_at: '2018-09-23'
  }
]

export default latest
