/**
 *
 * Header
 *
 */

import React from 'react'

import { Grid } from '@material-ui/core'
import { Link } from 'react-router-dom'

import latest from './mock/latest'

class Latest extends React.PureComponent {
  render () {
    return (
      <div>
        {latest.map((data, index) => {
          return (
            <div key={index} className="margin-latest">
              <span>
                <Grid className="paper" container spacing={8}>
                  <Grid className="pull-left" item xs={6}>
                    <h3 className="margin-mini">
                      <Link to="/home">
                        {data.title}
                      </Link>
                    </h3>
                  </Grid>
                  <Grid className="pull-right" item xs={6}>
                    <div className="margin-mini">
                      {data.release_at}
                    </div>
                  </Grid>
                </Grid>
              </span>
              {data.chapters.map((dataChapters, indexChapters) => {
                return (
                  <Link className="paper" to="/home" key={indexChapters}>
                    <div>
                      {`#${dataChapters.chapter}  ${dataChapters.title ? dataChapters.title : `chapter ${dataChapters.chapter}`}`}
                    </div>
                  </Link>
                )
              })}
            </div>
          )
        })}
      </div>
    )
  }
}

export default Latest
