const fs = require('fs')

const prettierOptions = JSON.parse(fs.readFileSync('./.prettierrc', 'utf8'))

module.exports = {
  extends: 'airbnb',
  plugins: ['redux-saga', 'react', 'jsx-a11y'],
  env: {
    browser: true,
    node: true,
    jest: true,
    es6: true,
  },
  parserOptions: {
    ecmaVersion: 8,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      experimentalObjectRestSpread: true
    },
  },
  rules: {
    "semi": [2, "never"],
    "camelcase": 0,
    "no-console": 0,
    "no-tabs": 0,
    "no-alert": 0,
    "comma-dangle": ["error", {
      "functions": "ignore"
    }],
    "max-len": 0,
    "react/forbid-prop-types": 0,
    "react/prop-types": 0,
    "react/jsx-first-prop-new-line": 0,
    "space-before-function-paren": [2, "always"],
    "no-unused-expressions": [0, {
      "allowShortCircuit": true,
      "allowTernary": true
    }],
    "arrow-body-style": [0, "never"],
    "func-names": 0,
    "prefer-const": 0,
    "no-extend-native": 0,
    "no-param-reassign": 0,
    "no-restricted-syntax": 0,
    "prefer-rest-params": 0,
    "no-eval": 0,
    "react/jsx-no-bind": 0,
    "no-unused-vars": [2, {
      "ignoreRestSiblings": true
    }],
    "implicit-arrow-linebreak": 0,
    "operator-linebreak": 0,
    "lines-between-class-members": 0,
    "import/order": 0,
    "react/destructuring-assignment": 0,
    "react/sort-comp": 0,
    "object-curly-newline": 0,
    "no-underscore-dangle": 0,
    "global-require": 0,
    "jsx-a11y/href-no-hash": "off",
    "jsx-a11y/anchor-is-valid": ["warn", {
      "aspects": ["invalidHref"]
    }],
    "react/jsx-filename-extension": [1, {
      "extensions": [".js", ".jsx"]
    }],
    "react/jsx-closing-tag-location": 0,
    "react/require-default-props": [1, {
      "forbidDefaultForRequired": true
    }],
    "jsx-a11y/no-static-element-interactions": 0,
    "react/prefer-stateless-function": 0,
    "react/react-in-jsx-scope": 0,
    "react/no-array-index-key": 0,
    "eslint.run": "onSave",
    "eslint.autoFixOnSave": true,
    "format_on_save": true,
    "guard-for-in": 0,
    "import/no-extraneous-dependencies": 0,
    "import/no-unresolved": 0,
    "import/extensions": 0,
    "import/prefer-default-export": 0,
    "no-cond-assign": 0,
    "consistent-return": 0,
    "no-nested-ternary": 0,
    "no-prototype-builtins": 0,
    "no-shadow": 0,
    "react/jsx-indent": ["warn", 2],
    "import/no-dynamic-require": 0,
    "import/no-named-as-default": 0,
    "import/no-webpack-loader-syntax": 0,
    "indent": ["warn", 2, {
      "SwitchCase": 1
    }]
  },
  parser: 'babel-eslint',
  globals: {
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: './internals/webpack/webpack.prod.babel.js',
      },
    },
  },
}
