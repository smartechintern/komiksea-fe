[![CircleCI](https://circleci.com/bb/smartechintern/komiksea-fe/tree/master.svg?style=svg&circle-token=50d9907e7eba27b6ef5d771d2f6e128b981d42e2)](https://circleci.com/bb/smartechintern/komiksea-fe/tree/master) [![Codacy Badge](https://api.codacy.com/project/badge/Grade/08ab091c537440b4a64f5fa0826ddd84)](https://www.codacy.com?utm_source=git@bitbucket.org&amp;utm_medium=referral&amp;utm_content=smartechintern/komiksea-fe&amp;utm_campaign=Badge_Grade)

## Quick start

1.  Make sure that you have Node v8 or above installed.
2.  Clone this repo using `git clone https://bitbucket.org/smartechintern/komiksea-fe`
3.  Move to the appropriate directory: `cd komiksea-fe`.<br />
4.  Run `npm run setup` in order to install dependencies and clean the git repo.<br />
    _We auto-detect `yarn` for installing packages by default, if you wish to force `npm` usage do: `USE_YARN=false npm run setup`_<br />
    _At this point you can run `npm start` to see the example app at `http://localhost:3000`._