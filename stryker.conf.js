module.exports = (config) => {
  config.set({
    testRunner: 'jest',
    mutator: 'javascript',
    transpilers: ['webpack'],
    reporters: ['html', 'baseline', 'clear-text', 'progress', 'dashboard'],
    packageManager: 'yarn',
    coverageAnalysis: 'off',
    mutate: ['app/containers/HomePage/**/*.js'],
    timeoutMS: 240000,
    webpack: {
      configFile: './internals/webpack/webpack.dev.babel.js'
    }
  })
}
